# docker-compose

To clone repository with all submoduals use
```
git clone --recurse-submodules -j8 git@gitlab.com:sp1_flask-app/SP1_flask-app.git
```

If submodules is outof sync use to update all of them
```
git submodule update --remote --merge
```